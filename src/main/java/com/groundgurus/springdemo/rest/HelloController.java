package com.groundgurus.springdemo.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author pgutierrez
 */
@RestController
@RequestMapping("/api")
public class HelloController {
    @GetMapping("hello")
    public String hello(
            @RequestParam(value = "name", defaultValue = "World")
            String name) {
        return "Hello " + name + "!";
    }
}
